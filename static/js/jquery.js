$(document).ready(function () {

    let hideMarkup = function() {
        let ids = ["id_regexp", 
                   "id_recursive_mode", 
                   "id_permanent_mode",
                ]
        let id = $(this).children("input").attr("id");
        if ($.inArray(id, ids) != -1) {
            $(this).hide();
        }
    }

    let getTrash = function() {
        $(".bin_content").remove();
        if ($("#id_action").val() == "RS") {
            $.ajax({
                url: `/content/${$("#id_trashbin").val()}/`,
            })
                .done(function(data) {
                    let list = "<div class=bin_content><ul>";
                    let content = JSON.parse(data);
                    for (let i = 0; i < content.length; i++) {
                        if (content[0][0] == null && content[0][1] == null) {
                            list += 
                                `<li class=empty>
                                    <p>Empty</p>
                                </li>`;
                            break;
                        }
                        list += 
                            `<li class=restore>
                                <input type=checkbox name="cb${i}" value="${content[i][1]}"/>
                                <div class=bin_item> 
                                    <p class=source> Source path:"${content[i][0]}"</p> 
                                    <p class=dest> Destination path:"${content[i][1]}"</p>
                                </div>
                            </li>`;
                    }
                    list += "</ul></div>";

                    $("p:has(select)").each(function() {
                        let id = $(this).children("select").attr("id");
                        if (id != "id_trashbin") {
                            return;
                        }
                        $(this).after(list);
                    });
                });   
        }
    }


    let getDirContent = function(that) {
        $(that).val("-");
        $(that).text("-");
        
        $.ajax({
            url: `/files/1${$(that).attr("name")}/`,
        })
        .done(function(data) {
                btn_name = $(that).attr("name");
                tag = btn_name.split(/[/() ,_]/).join("");
                let markup = `<div class=subdirs id='${tag}'>`;
                let content = JSON.parse(data);
                for (let key in content[0]) {
                    markup += 
                            `<div class=dir>
                                <button class=expander type=button name="${key} value=+>+</button> 
                                <input type=checkbox value="${key}" name="${key}"/>
                                <label for="${key}">${content[0][key]}
                            </div>`; 
                }   
                if ($("#id_regexp").val() == "") {
                    for (key in content[1]) {
                        markup += 
                            `<div class=file>
                                <input type=checkbox value="${key}" name="${key}">
                                <label for="${key}"> ${content[1][key]}
                            </div>`;
                    }
                }
                markup += "</div>";
                
                $("label").each(function() {
                    if ($(this).attr("for") == btn_name) {
                        $(this).after(markup);
                        return false;
                    }
                });
            });
    }

    let hideContent = function(that) {
        event.preventDefault();
        tag = "#" + $(that).attr("name").split(/[/() ,_]/).join("");
        console.log($(tag));
        $(tag).remove();
        $(that).val("+");
        $(that).text("+");
        
    }

    let clickHandler = function(event) {
        if ($(this).val() == "+") {
            getDirContent(this);
        }
        else if ($(this).val() == "-") {
            hideContent(this);
        }

    }

    $("#id_action").change(function() {
        if ($(this).val() == 'RS') {
            $(".filetree").hide();
            $("p").each(hideMarkup)
            getTrash();

            $("#id_trashbin").change(getTrash);
        }
        else {
            $(".filetree").show();
            $(".bin_content").remove();
            $("p").each(function() {
                $(this).show();
            });

        }   
    });

    $("#id_regexp").on("change", function() {
        if ($(this).val() != "")
            $(".file").hide();
        else
            $(".file").show();
    });
    
    $(".filetree").on("click", "button", clickHandler);

    if ($("#capacity").length !== 0) {
        $("#id_path_to_storage").prop("readonly", true);
    }
});




