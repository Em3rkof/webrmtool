# WEBRmtool 0.5

Small UNIX OS utilite providing removing/restoring opeartions with file storage support. Uses web interface.

Developed on python 2.7 & Django 1.11.4

## Requirements:

### !! You should have *nginx*, *uwsgi* and *MySQL* pre-installed !!

**1)** To install this piece of software, run:

``` bash
    pip install uwsgi
    pip install MySQL-python
    sudo apt-get install nginx
```

**2)** Then make a symlink to your project's nginx *.conf* file in */etc/nginx/sites-enabled/*:
(It's condidered you've already created and set up all the nginx configuration files in your project)

``` bash
    ln -s [path-to-your-project_nginx.conf-file] [your-project_nginx.conf]

```

**3)** Create MySQL database for your project

**4)** Insert in the end of */etc/mysql/my.cnf* following section:

``` bash
    [client]
    database = [name of databse you've created in 3)]
    host = [specify host]
    user = [specify user]
    password = [specify user's password]
    default-character-set = [specify charset]
```

and in *YouProject* **settings.py**:

```python
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'OPTIONS': {
                'read_default_file': '/etc/mysql/my.cnf'
            }
        }   
    }
```

**5)** **Cut** a *SECRET_KEY* from *setting.py* and create **secret.py** in the same folder.

Then:

In *secret.py*:
```python
    S_KEY = [your_secret_key]
```

In *settings.py*:
```python
    from secret import S_KEY
    ...
    SECRET_KEY = S_KEY
```

## Usage guide:

To run server, type in webrm project folder:

``` bash
--uwsgi --ini webrm_uwsgi.ini
```

To activate web-interface, type

``` bash
127.0.0.1:[port-you've configured]
```
in your web-browser adress input.