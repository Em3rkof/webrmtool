from django.conf.urls import url
from django.shortcuts import render, redirect, get_object_or_404
from bin_manager import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^factory/$', views.factory, name='factory'),
    url(
    	r'^trashbin/(?P<pk>[0-9]+)/$', 
        views.trashbin_detail,
        name='trashbin_detail'
    ),
]