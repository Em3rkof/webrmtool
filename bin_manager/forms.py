# -*- coding: utf-8 -*-
from django import forms
from bin_manager.models import Trashbin

class BinForm(forms.ModelForm):

    error_css_class = 'error'

    class Meta:
        model = Trashbin
        fields = ('path_to_storage',
                  'storage_size',
                  'policy_max_size',
                  'policy_max_file_count',
                  'policy_max_store_time',
                 )
