# -*- coding: utf-8 -*-
import os
from django.core.exceptions import ValidationError

def validate_path(value):
  if not os.path.expanduser(value).startswith('/home') or os.path.exists(value):
      raise ValidationError('Invalid path parameter.')

def validate_digit(value):
    if value < 0:
        raise ValidationError(
            ('%(value)s should be a positive number!'),
            params={'value': value},
        )