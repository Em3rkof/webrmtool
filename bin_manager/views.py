# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect, get_object_or_404
from bin_manager.models import Trashbin
from bin_manager.forms import BinForm


FIELDS = [
    'path_to_storage',
    'storage_size',
    'policy_max_size',
    'policy_max_file_count',
    'policy_max_store_time',
    ]

def index(request):
    trashbin_obj_list = Trashbin.objects.order_by()
    context = {'bin_list': trashbin_obj_list}
    return render(request, 'bin_manager/index.html', context)

def factory(request):
    if request.method == 'POST':
        form = BinForm(request.POST)

        if form.is_valid():
            trashbin = form.save()
            trashbin.save()
            trashbin.create_trashbin()
            return redirect('index')
    else:
        form = BinForm()

    return render(request,
                  'bin_manager/factory.html',
                  {
                      'form': form
                  },
                 )

def trashbin_detail(request, pk):
    trashbin = get_object_or_404(Trashbin, pk=pk)
    capacity = trashbin.count_files()
    if request.method == 'POST':

        if 'save' in request.POST:
            form = BinForm(request.POST)
            if form.is_valid():
                new_data = _filter(request.POST, FIELDS)
                Trashbin.objects.filter(id=pk).update(**new_data)
        elif 'remove' in request.POST:
            trashbin.remove_trashbin()
            trashbin.delete()
            return redirect('index')

    else:
        defaults = dict(zip(FIELDS, _getattrs(trashbin, FIELDS)))
        form = BinForm(initial=defaults)
        trashbin.apply_policies()

    return render(
        request,
        'bin_manager/trashbin_detail.html',
        {
            'trashbin': trashbin,
            'form': form,
            'capacity': capacity,
        },
    )

def _filter(dictionary, keys):
    return {key: item for key, item in dictionary.iteritems() if key in keys}

def _getattrs(obj, attrs):
    return tuple(getattr(obj, attr) for attr in attrs)
