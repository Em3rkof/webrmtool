# -*- coding: utf-8 -*-
import os
import shutil

from bin_manager.models import Trashbin
from bin_manager.forms import BinForm

from django.urls import reverse
from django.test import TestCase


def create_trashbin(
        path_to_storage=os.path.expanduser('~/TESTBIN'),
        storage_size=1000,
        policy_max_size=1000,
        policy_max_file_count=5000,
        policy_max_store_time=100000000):
    return Trashbin.objects.create(
        path_to_storage=path_to_storage,
        storage_size=storage_size,
        policy_max_size=policy_max_size,
        policy_max_file_count=policy_max_file_count,
        policy_max_store_time=policy_max_store_time,
    )


class TrashbinListViewTest(TestCase):
    def test_no_trashbin(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(
            response,
            'There\'re no trashbins created..still.'
        )
        self.assertQuerysetEqual(response.context['bin_list'],
                                 [])

    def test_trashbin_display(self):
        create_trashbin()
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['bin_list'],
                                 ['<Trashbin: {}/TESTBIN>'.format(
                                     os.path.expanduser('~')
                                     )
                                 ]
                                )

        def tearDown(self):
            if os.path.exists(os.path.expanduser('~/TESTBIN')):
                shutil.rmtree(os.path.expanduser('~/TESTBIN'))


class TrashbinCreationViewTest(TestCase):
    def test_display(self):
        response = self.client.get(reverse('factory'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(
            response,
            'Create a new trashbin!'
        )


class TrashbinDetailViewTest(TestCase):
    def test_empty(self):
        trashbin = create_trashbin()
        url = reverse('trashbin_detail', args=(trashbin.id, ))
        response = self.client.get(url)
        self.assertContains(response, 'Empty')

    def tearDown(self):
        if os.path.exists(os.path.expanduser('~/TESTBIN')):
            shutil.rmtree(os.path.expanduser('~/TESTBIN'))


class TrashbinFormTest(TestCase):
    def setUp(self):
        self.form_data = {
            'path_to_storage': os.path.expanduser('~/TESTBIN'),
            'storage_size': 1000,
            'policy_max_size': 1000,
            'policy_max_file_count': 5000,
            'policy_max_store_time': 1000000.0,
            }

    def test_correct_data(self):
        form = BinForm(self.form_data)
        print form.errors
        self.assertTrue(form.is_valid())

    def test_negative_digit(self):
        self.form_data['storage_size'] = -1000
        self.form_data['policy_max_file_count'] = -5000
        form = BinForm(data=self.form_data)
        self.assertFalse(form.is_valid())

    def test_path_exists(self):
        self.form_data['path_to_storage'] = os.path.expanduser('~/BIN')
        os.mkdir(self.form_data['path_to_storage'])
        form = BinForm(data=self.form_data)
        self.assertFalse(form.is_valid())

    def tearDown(self):
        if os.path.exists(self.form_data['path_to_storage']):
            shutil.rmtree(self.form_data['path_to_storage'])


class TrashbinModelTest(TestCase):
    def test_create_trashbin(self):
        create_trashbin()
        self.assertQuerysetEqual(Trashbin.objects.all(),
                                 ['<Trashbin: {}/TESTBIN>'.format(
                                     os.path.expanduser('~')
                                     )
                                 ]
                                )

    def tearDown(self):
        if os.path.exists(os.path.expanduser('~/TESTBIN')):
            shutil.rmtree(os.path.expanduser('~/TESTBIN'))
