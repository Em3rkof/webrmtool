# -*- coding: utf-8 -*-

import os
import shutil

from rmtool.trashbin import Trashbin as TrashBin

from django.db import models
from bin_manager.validators import validate_digit, validate_path


class Trashbin(models.Model):
    """Represents file storage model"""
    path_to_storage = models.CharField('Creation path',
                                       max_length=256,
                                       validators=[validate_path],
                                      )
    storage_size = models.PositiveIntegerField('Storage size(MB)')
    policy_max_size = models.PositiveIntegerField('Max content size(MB)')
    policy_max_file_count = models.PositiveIntegerField('Max file count')
    policy_max_store_time = models.FloatField('Max store time(hours)',
                                              validators=[validate_digit])
    dryrun_mode = models.BooleanField(default=False)

    def __str__(self):
        return self.path_to_storage

    def create_trashbin(self):
        """Creates a file storage folder and info file within it """
        return TrashBin(
            path_to_storage=self.path_to_storage,
            storage_size=self.storage_size,
            policy_max_size=self.policy_max_size,
            policy_max_store_time=self.policy_max_store_time,
            policy_max_file_count=self.policy_max_file_count,
            dryrun_mode=self.dryrun_mode,
        )

    def get_content(self):
        return self.create_trashbin().get_content()

    def count_files(self):
        return self.create_trashbin().count_items()

    def get_size(self):
        return self.create_trashbin().get_size()

    def apply_policies(self):
        self.create_trashbin().apply_policy()

    def restore_files(self, paths):
        return self.create_trashbin().silent_restore(paths)

    def remove_trashbin(self):
        shutil.rmtree(os.path.expanduser(self.path_to_storage))

