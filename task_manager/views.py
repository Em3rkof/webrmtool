# -*- coding: utf-8 -*-
import os
import json

from django.shortcuts import render, redirect, get_object_or_404, HttpResponse
from task_manager.forms import TaskForm
from task_manager.models import Task
from task_manager.constants import CODE, DESTINATION_PATH, NEXT_DATA_SEC
from bin_manager.models import Trashbin

def task_factory(request):
    if not Trashbin.objects.all():
        return redirect('index')

    if request.method == 'POST':
        form = TaskForm(request.POST)
        trashbin = Trashbin.objects.get(id=request.POST['trashbin'])
        paths = []

        if form.is_valid():
            task = form.save()
            trashbin.dryrun_mode = task.dryrun_mode

            for key, value in request.POST.items():
                if key.startswith('cb') or key.startswith('/home'):
                    paths.append(value)
                else:
                    continue

            if task.action == 'RM':
                regexp = task.regexp if task.regexp else None
                results = task.remove(paths, regexp)
            else:
                results = trashbin.restore_files([os.path.basename(path) for path in paths])

            for result in results:
                for error, source, dest in result:
                    code = error[0]
                    msg = error[1]
                    task.paths += "{code}|{msg}|{source}|{dest}|".format(
                        code=code,
                        msg=msg.format(source),
                        source=source,
                        dest=dest,
                    )
            task.paths = task.paths[:-1]
            task.save()
            return redirect('task_list')

    else:
        form = TaskForm()
        dirs = _filter_dirs(os.path.expanduser("~/"))
        files = _filter_files(os.path.expanduser("~/"))

        return render(request,
                      'task_manager/task_factory.html',
                      {
                          'root': dirs,
                          'files': files,
                          'form': form
                      },
                     )

def get_content(request, pk):
    trashbin = Trashbin.objects.get(id=pk)
    content = trashbin.get_content()
    json_content = json.dumps(content)

    return HttpResponse(json_content,
                        content_type="application/javascript")

def get_dir_content(request, folder):
    folder = folder[1:]
    json_content = "["
    json_content += json.dumps(_filter_dirs(folder))
    json_content += ", "
    json_content += json.dumps(_filter_files(folder))
    json_content += "]"

    return HttpResponse(json_content,
                        content_type="application/javascript")

def task_list(request):
    tasks = Task.objects.order_by('-id')
    codes = {}
    for task in tasks:
        res = task.paths.split('|')
        code_index = CODE
        codes[task.id] = 0
        while code_index < len(res) - 1:
            if int(res[code_index]) > 0:
                codes[task.id] = 1
            code_index += NEXT_DATA_SEC

    context = {
        'task_list': tasks,
        'codes': codes,
        }

    return render(request,
                  'task_manager/task_list.html',
                  context)

def task_detail(request, pk):
    task = get_object_or_404(Task, pk=pk)
    lst = _extract_info(task.paths.split('|'))

    if request.method == 'POST':

        if 'remove' in request.POST:
            Task.objects.get(id=pk).delete()
            return redirect('task_list')
    else:
        form = TaskForm()

        return render(request,
                      'task_manager/task_detail.html',
                      {
                          'task': task,
                          'form': form,
                          'res': lst,
                      },
                     )

def _filter_dirs(path):
    return {
        os.path.join(path, item):
        item for item in os.listdir(path)
        if os.path.isdir(os.path.join(path, item))
        and item[0] != '.' and os.path.join(path, item) not in
        [os.path.expanduser(elem.path_to_storage)
         for elem in Trashbin.objects.all()]
    }

def _filter_files(path):
    return {
        os.path.join(path, item):
        item for item in os.listdir(path)
        if os.path.isfile(os.path.join(path, item))
        and item[0] != '.'
    }

def _extract_info(op_result):
    """Creates a list of lists, where every 1st item is
    error code, 2nd - error msg, 3rd - source file path,
    4th - dest file path(if removed to trash) """
    data_section = 0
    item_offset = 0
    lst = []
    temp = []

    while data_section < len(op_result) - 1:
        if item_offset == CODE:
            temp.append(int(op_result[data_section + item_offset]))
        elif item_offset == DESTINATION_PATH:
            temp.append(op_result[data_section + item_offset])
            lst.append(temp)
            temp = []
        else:
            temp.append(op_result[data_section + item_offset])

        item_offset += 1

        if item_offset == NEXT_DATA_SEC:
            data_section += NEXT_DATA_SEC
            item_offset = 0

    return lst
