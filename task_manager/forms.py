# -*- coding: utf-8 -*-
import os

from django import forms
from task_manager.models import Task


class TaskForm(forms.ModelForm):
    error_css_class = 'error'
    regexp = forms.CharField(required=False)

    class Meta:
        model = Task
        fields = ('action',
                  'trashbin',
                  'regexp',
                  'recursive_mode',
                  'dryrun_mode',
                  'permanent_mode',
                 )