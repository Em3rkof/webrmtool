# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-21 17:29
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('task_manager', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='paths',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='task',
            name='trashbin',
            field=models.ForeignKey(default=2L, on_delete=django.db.models.deletion.CASCADE, to='bin_manager.Trashbin'),
        ),
    ]
