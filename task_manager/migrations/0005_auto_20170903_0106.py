# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-02 22:06
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('task_manager', '0004_auto_20170828_0230'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='trashbin',
            field=models.ForeignKey(default=8L, on_delete=django.db.models.deletion.CASCADE, to='bin_manager.Trashbin'),
        ),
    ]
