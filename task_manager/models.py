# -*- coding: utf-8 -*-
from rmtool.recycler import Recycler

from django.db import models
from bin_manager.models import Trashbin

class Task(models.Model):
    REMOVE = 'RM'
    RESTORE = 'RS'

    ACTIONS = (
        (REMOVE, 'Remove'),
        (RESTORE, 'Restore'),
    )

    action = models.CharField(max_length=2,
                              choices=ACTIONS,
                              default=REMOVE,
                             )
    trashbin = models.ForeignKey(Trashbin,
                                 blank=False,
                                 on_delete=models.CASCADE,
                                 default=Trashbin.objects.first().id
                                         if Trashbin.objects.all()
                                         else None
                                )
    regexp = models.CharField(max_length=256)
    recursive_mode = models.BooleanField()
    dryrun_mode = models.BooleanField()
    permanent_mode = models.BooleanField()
    paths = models.TextField(default="")

    def __str__(self):
        return  '{} {}'.format(self.action, str(self.trashbin))

    def remove(self, paths, regexp=None):
        recycler = Recycler(
            path_to_storage=self.trashbin.path_to_storage,
            storage_size=self.trashbin.storage_size,
            recursive_mode=self.recursive_mode,
            permanent_mode=self.permanent_mode,
            dryrun_mode=self.dryrun_mode)
        return recycler.launch_rm(paths, regexp=regexp)
