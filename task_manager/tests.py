# -*- coding: utf-8 -*-
import os
import shutil

from django.urls import reverse
from django.test import TestCase
from task_manager.models import Task
from bin_manager.models import Trashbin


def create_trashbin(
        path_to_storage=os.path.expanduser('~/TESTBIN'),
        storage_size=1000,
        policy_max_size=1000,
        policy_max_file_count=5000,
        policy_max_store_time=100000000):
    return Trashbin.objects.create(
        path_to_storage=path_to_storage,
        storage_size=storage_size,
        policy_max_size=policy_max_size,
        policy_max_file_count=policy_max_file_count,
        policy_max_store_time=policy_max_store_time,
    )

def create_task(
        action='RM',
        trashbin=None,
        recursive_mode=True,
        dryrun_mode=False,
        permanent_mode=False,
        paths=""):
    trashbin = create_trashbin()
    return Task.objects.create(
        action=action,
        trashbin=trashbin,
        recursive_mode=recursive_mode,
        dryrun_mode=dryrun_mode,
        permanent_mode=permanent_mode,
        paths=paths,
    )

def update_paths_field(task, paths):
    for result in paths:
        for error, source, dest in result:
            code = error[0]
            msg = error[1]
            task.paths += "{code}|{msg}|{source}|{dest}| ".format(
                code=code,
                msg=msg.format(source),
                source=source,
                dest=dest,
            )
    task.paths = task.paths[:-1]
    task.save()


class TaskListViewTest(TestCase):
    def test_no_tasks(self):
        response = self.client.get(reverse('task_list'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response,
                            'There\'re no tasks created..still.'
                           )
        self.assertQuerysetEqual(response.context['task_list'],
                                 [])

    def test_empty_db(self):
        response = self.client.get(reverse('task_factory'))
        self.assertEqual(response.status_code, 302)

    def test_task_display(self):
        create_task()
        response = self.client.get(reverse('task_list'))
        self.assertEqual(response.status_code, 200)

        self.assertQuerysetEqual(response.context['task_list'],
                                 ['<Task: RM {}/TESTBIN>'.format(
                                     os.path.expanduser('~')
                                     )
                                 ]
                                )


class TaskCreationViewTest(TestCase):
    def setUp(self):
        create_trashbin()

    def test_display(self):
        response = self.client.get(reverse('task_factory'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response,
                            'Create a new task!'
                           )


class TaskDetailViewTest(TestCase):
    def setUp(self):
        self.paths = [os.path.expanduser('~/5/6')]
        for path in self.paths:
            os.makedirs(path)

    def test_error_removal(self):
        task = create_task(recursive_mode=False)
        results = task.remove([os.path.expanduser('~/5')])
        update_paths_field(task, results)
        url = reverse('task_detail', args=(task.id, ))
        response = self.client.get(url)
        self.assertContains(response, 'Error while removing')

    def test_success_removal(self):
        task = create_task()
        results = task.remove([os.path.expanduser('~/5')])
        update_paths_field(task, results)
        url = reverse('task_detail', args=(task.id, ))
        response = self.client.get(url)
        self.assertContains(response, 'Done')

    def tearDown(self):
        if os.path.exists(os.path.expanduser('~/TESTBIN')):
            shutil.rmtree(os.path.expanduser('~/TESTBIN'))
        for path in self.paths:
            if os.path.exists(path):
                shutil.rmtree(path)
