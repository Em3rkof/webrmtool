from django.conf.urls import url
from django.shortcuts import render, redirect, get_object_or_404
from task_manager import views

urlpatterns = [
    url(r'^task_factory/$', views.task_factory, name='task_factory'),
        url(r'content/(?P<pk>[0-9]+)/$', 
            views.get_content, 
            name='get_content'),
        url(r'files/(?P<folder>.+)/$', 
            views.get_dir_content, 
            name='get_dir_content'),
    url(r'^task_list/$', views.task_list, name='task_list'),
    url(r'^task/(?P<pk>[0-9]+)/$',
        views.task_detail,
        name='task_detail'),
]